from tokenize import Token
import torch.nn as nn
from torch import Tensor
from typing import Optional
import torch
import math

import helper as h
from Dictionary import Dictionary


## PARAMETERS -------------------------------------------------------------------------------------

MODE = 'translate'
MODEL_PATH = "/content/models_with_layer+dropout+lr.3/"
BEAMSIZE = 5
TRAIN_ON_ONLY_PART_OF_TEXT = False
PART_OF_TEXT = 5000


# Files with BPE split (and tokenized) texts
dict_source = "multi30k_de_7k"
dict_target = "multi30k_en_7k"

SRC_LANGUAGE = 'de'
TGT_LANGUAGE = 'en'

# Generate dictionaries
dict_de = Dictionary(dict_source)
dict_en = Dictionary(dict_target)

# Training and evaluation text files
source_file_train = "multi30k_de_7k"               
target_file_train = "multi30k_en_7k"          

source_file_eval = "multi30k_dev_de_7k"
target_file_eval = "multi30k_dev_en_7k"

# Define special symbols and indices
BOS_IDX, EOS_IDX, UNK_IDX, PAD_IDX = 0, 1, 2, 3

DEVICE = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# Model parameters
SRC_DICT_SIZE = dict_de.len()
TGT_DICT_SIZE = dict_en.len()
EMB_SIZE = 512
NHEAD = 8
HID_DIM = 512
BATCH_SIZE = 128



## NETWORK ----------------------------------------------------------------------------------------
class RNNetwork(nn.Module):

    def __init__(self, emb_size: int, hid_dim: int, nhead: int, src_dict_size: int, tgt_dict_size: int, dropout_p=0.1):

        super(RNNetwork, self).__init__()


        self.src_emb = nn.Embedding(src_dict_size, emb_size)
        self.tgt_emb = nn.Embedding(tgt_dict_size, emb_size)

        self.encoder = Encoder(emb_size, hid_dim)
        self.decoder = Decoder(emb_size, hid_dim, nhead, tgt_dict_size)

        #test for dropout
        self.dropout_p = dropout_p
        self.dropout = nn.Dropout(self.dropout_p)

    def forward(self, src: Tensor, tgt: Tensor) -> Tensor:
        
        src_emb = self.src_emb(src)
        tgt_emb = self.tgt_emb(tgt)

        src_emb = self.dropout(src_emb)
        tgt_emb = self.dropout(tgt_emb)

        memory = self.encoder(src_emb)
        output = self.decoder(tgt_emb, memory)

        return output

    def encode(self, src: Tensor):
        return self.encoder(src)

    def decode(self, tgt: Tensor, memory: Tensor):
        return self.decoder(tgt, memory)

class Encoder(nn.Module):

    def __init__(self, emb_size: int, hid_dim: int):
        super(Encoder, self).__init__()

        self.linear = nn.Linear(emb_size, hid_dim)
        self.activation = nn.ReLU()
        self.lstm = nn.LSTM(hid_dim, hid_dim)
        self.norm = nn.LayerNorm(hid_dim)

    def forward(self, src: Tensor) -> Tensor:
        x = src
        x = self.linear(x)
        x = self.activation(x)
        x, _ = self.lstm(x)
        x = self.norm(x)

        return x

class Decoder(nn.Module):

   def __init__(self, emb_size: int, hid_dim: int, nhead: int, tgt_dict_size: int):
        super(Decoder, self).__init__()

        self.linear1 = nn.Linear(emb_size, hid_dim)
        self.lstm = nn.LSTM(hid_dim, hid_dim)
        self.norm1 = nn.LayerNorm(hid_dim)

        self.multihead_attn = nn.MultiheadAttention(hid_dim, nhead)
        self.norm2 = nn.LayerNorm(hid_dim)
        self.linear2 = nn.Linear(hid_dim, tgt_dict_size)

        self.activation = nn.ReLU()

   def forward(self, tgt: Tensor, memory: Tensor) -> Tensor:
        x = tgt
        x = self.linear1(x)
        x = self.activation(x)
        x, _ = self.lstm(x)
        x = self.norm1(x)

        y, _ = self.multihead_attn(x, memory, memory)

        z = torch.add(x, y)
        z = self.norm2(z)
        z = self.linear2(z)

        return z




## COLLATION --------------------------------------------------------------------------------------
from torch.nn.utils.rnn import pad_sequence

# Function that strips \n from sentences, replaces tokens with indices, adds BOS and EOS and returns tensor
def prepSentence(sentence, dict):
    token_ids = [dict.getIndex(word) for word in sentence.split()]
    tensor = torch.cat((torch.tensor([BOS_IDX]),
                        torch.tensor(token_ids),
                        torch.tensor([EOS_IDX])))
    return tensor

# function to collate data samples into batch tensors
def collate_fn(batch):
    src_batch, tgt_batch = [], []
    for src_sample, tgt_sample in batch:
        src_batch.append(prepSentence(src_sample, dict_de))
        tgt_batch.append(prepSentence(tgt_sample, dict_en))

    src_batch = pad_sequence(src_batch, padding_value=PAD_IDX)
    tgt_batch = pad_sequence(tgt_batch, padding_value=PAD_IDX)
    return src_batch, tgt_batch



## DATASET ----------------------------------------------------------------------------------------
from torch.utils.data import Dataset
from torch.utils.data import DataLoader

class OurDataset(Dataset):
    def __init__(self, source_file_name: str, target_file_name: str):
        lines_de = h.extractText(source_file_name).splitlines()
        lines_en = h.extractText(target_file_name).splitlines()
        
        # For testing we can use only 1000 lines so that the training is faster
        if TRAIN_ON_ONLY_PART_OF_TEXT == True:
            lines_de = lines_de[0:PART_OF_TEXT] 
            lines_en = lines_en[0:PART_OF_TEXT]

        self.source_data = []
        self.target_data = []
        for line_de, line_en in zip(lines_de, lines_en):
            self.source_data.append( line_de.rstrip("\n") )
            self.target_data.append( line_en.rstrip("\n") )
    
    def __getitem__(self, index: int):
        return self.source_data[index], self.target_data[index]
    
    def __len__(self):
        return len(self.source_data)



## TRAINING AND EVALUATION ------------------------------------------------------------------------
# Trains model on given text
def train_epoch(model, optimizer):
    model.train()
    losses, correct, num_of_words = 0, 0, 0
    train_iter = OurDataset(source_file_train, target_file_train)
    train_dataloader = DataLoader(train_iter, batch_size=BATCH_SIZE, shuffle=True, collate_fn=collate_fn)

    for src, tgt in train_dataloader:
        src = src.to(DEVICE)
        tgt = tgt.to(DEVICE)

        tgt_input = tgt[:-1, :]

        # Calculate model output
        logits = model(src, tgt_input)

        # Calculate loss, backpropagate and update model parameters
        optimizer.zero_grad()

        tgt_out = tgt[1:, :]
        loss = loss_fn(logits.reshape(-1, logits.shape[-1]), tgt_out.reshape(-1))
        loss.backward()
        optimizer.step()

        # Store loss
        losses += loss.item()
        
        # Calculate accuracy and store
        correct_batch, num_of_words_batch = calculateAcc(tgt_out, logits)
        correct += correct_batch
        num_of_words += num_of_words_batch

    accuracy = ( correct / num_of_words ) * 100

    return losses / len(train_dataloader) , accuracy

# Evaluates model performance on a text
def evaluate(model):
    model.eval()
    losses, correct, num_of_words = 0, 0, 0

    val_iter = OurDataset(source_file_eval, target_file_eval)
    val_dataloader = DataLoader(val_iter, batch_size=BATCH_SIZE, shuffle=True, collate_fn=collate_fn)

    for src, tgt in val_dataloader:
        src = src.to(DEVICE)
        tgt = tgt.to(DEVICE)

        tgt_input = tgt[:-1, :]

        # Calculate model output
        logits = model(src, tgt_input)

        # Calculate loss and store loss
        tgt_out = tgt[1:, :]
        loss = loss_fn(logits.reshape(-1, logits.shape[-1]), tgt_out.reshape(-1))
        losses += loss.item()

        # Calculate accuracy and store
        correct_batch, num_of_words_batch = calculateAcc(tgt_out, logits)
        correct += correct_batch
        num_of_words += num_of_words_batch

    accuracy = ( correct / num_of_words ) * 100

    return losses / len(val_dataloader), accuracy

# Calculates accuracy for given output and labels
def calculateAcc(tgt_out: Tensor, logits: Tensor):
    correct, num_of_words = 0, 0
    tgt_out_acc = tgt_out.transpose(0, 1)

    for ln_idx in range(len(tgt_out_acc)):
        for index in range(len(tgt_out_acc[ln_idx])):
            if tgt_out_acc[ln_idx][index] == PAD_IDX:
                num_of_words += index
                break
            else:
                correct += (logits[index][ln_idx].argmax() == tgt_out_acc[ln_idx][index])

                # Edge case for the one sentence in batch that is longest and has no <pad>
                if index+1 == len(tgt_out_acc[ln_idx]):
                    num_of_words += index+1

    return correct, num_of_words



## BEAM SEARCH & TRANSLATION ----------------------------------------------------------------------
import bpe
import torch.nn.functional as F

# Beam search for one sentence
def beamsearch(model, sentence: str, beamsize: int, max_len: int):
    
    k = beamsize

    # Initialize result tensor and tensor that holds probabilities for current top k hyps
    e_hat = torch.ones(k, 1).fill_(BOS_IDX)
    e_probs = torch.ones(k, 1).float()

    i = 0

    # First iteration
    i += 1
    s_tensor = prepSentence(sentence, dict_de).to(DEVICE)
    memory = model.encode(model.src_emb(s_tensor).to(DEVICE))
    logits = model.decode(model.tgt_emb(e_hat[0].int().to(DEVICE)), memory)
    probs = F.softmax(logits)
    values, indices = torch.topk(probs[0], k)
    
    e_hat_new = list()
    for j in range(k):
        temp = e_hat[j].int().tolist()
        temp.append( indices[j].item() )
        e_hat_new.append( temp )
        e_probs[j] *= values[j].item()
    e_hat = torch.tensor(e_hat_new)

    # Following iterations
    for i in range(1, max_len-1):

        # Variable to save the k top-k 
        current = []

        # Iterate through all of the k current hypotheses
        for j in range(k):

            # Calculate top k predicted indices for one current hypothesis
            logits = model.decode(model.tgt_emb(e_hat[j].int().to(DEVICE)), memory)
            probs = F.softmax(logits[-1])
            values, indices = torch.topk( probs, k)

            # Multiplicate all values each with last probability
            values = values * e_probs[j].to(DEVICE)

            for v, idx in zip(values.tolist(), indices.tolist()):
                current.append([j, v, idx])
            
        # Extract only the probability values from current and determine topk
        _, indices = torch.topk(torch.tensor(current)[:, 1] , k)

        e_hat_new = list()
        e_probs_new = list()

        # Go through highest ranking hypotheses
        for idx in indices:

            # Look up corresponding token (index)
            token = current[idx][2]

            # Look up corresponding target history
            history = e_hat[current[idx][0]].tolist()
            history.append(token)            

            e_hat_new.append(history)
            e_probs_new.append(current[idx][1])

        # Update e_hat and e_probs
        e_hat = torch.tensor(e_hat_new)
        e_probs = torch.tensor(e_probs_new)

        if EOS_IDX in e_hat[:, -1]:
            break

    return e_hat.tolist()

# Helper for beam search result to put all sentences in one array and add empty lines
def make2D(text):
    temp = list()
    for line in text:
        for hyp in line:
            temp.append(hyp)
        temp.append([dict_en.getIndex("<s>")])
    return temp

# Handles search for a whole text
def translate(text: str, model, beamsize: int, onlybest: bool = True, post: bool = True):
    lines = text.splitlines()
    encoded_text = list()

    # Beam search for every sentence
    for line in lines:
        hyps = beamsearch(model, line, beamsize, len(line.split()) + 5)
        encoded_text.append(hyps)

    encoded_text = make2D(encoded_text)

    # Decode text as in replace indices with according words
    decoded_text = list()
    for i in range(len(encoded_text)):
        decoded_text.append([])
        for j in range(len(encoded_text[i])):
            word = dict_en.getWord(encoded_text[i][j])
            decoded_text[i].append(word)

    # Rebuild text from table
    translation = h.rebuildText2D(decoded_text)

    # Remove <s> and </s> symbols
    translation = translation.replace("<s>", "").replace("<s> ", "").replace("</s>","")

    # Remove BPE if postprocessing is asked for
    if post:
        translation = bpe.bpe_undo(translation)
    
    # Only give back best translation
    if onlybest:
        lines = translation.splitlines()
        best = lines[::beamsize+1]
        translation = "\n".join(best)
    
    return translation
        



if __name__ == "__main__":

    ## MODEL TRAINING ---------------------------------------------------------------------------------
    if MODE == 'train':

        # Model parameters
        torch.manual_seed(1)

        network = RNNetwork(EMB_SIZE, HID_DIM, NHEAD, SRC_DICT_SIZE, TGT_DICT_SIZE)

        for p in network.parameters():
            if p.dim() > 1:
                nn.init.xavier_uniform_(p)         

        network = network.to(DEVICE)

        loss_fn = torch.nn.CrossEntropyLoss(ignore_index=PAD_IDX)
        lr = 0.001
        optimizer = torch.optim.Adam(network.parameters(), lr=lr)
        scheduler = torch.optim.lr_scheduler.StepLR(optimizer, 1.0, gamma=0.5)

        # Training loop
        from timeit import default_timer as timer
        NUM_EPOCHS = 10

        for epoch in range(1, NUM_EPOCHS+1):
            start_time = timer()
            train_loss, train_acc = train_epoch(network, optimizer)
            end_time = timer()
            eval_loss, eval_acc = evaluate(network)

            train_perplexity = math.exp(train_loss)
            eval_perplexity = math.exp(eval_loss)

            latest_lr = scheduler.get_last_lr()[0]

            print((f"Epoch: {epoch}, Epoch time = {(end_time - start_time):.3f}s"))
            print((f"Learning rate: {latest_lr}"))
            print((f"Train loss: {train_loss:.3f}, Train perplexity: {train_perplexity:.3f}, Train accuracy: {train_acc:.3f}"))
            print((f"Eval loss: {eval_loss:.3f}, Eval perplexity: {eval_perplexity:.3f}, Eval accuracy: {eval_acc:.3f}"))
            print("")

            # Save net
            path = MODEL_PATH + str(epoch) + ".pth"
            torch.save(network, path)

            if epoch%3 == 0: 
              #update the lr
              scheduler.step()



    ## TRANSLATION WITH TRAINED MODEL -----------------------------------------------------------------
    if MODE == 'translate':

        source_text = h.extractText(source_file_eval)

        #for i in range(1,11):
          # model = torch.load("/content/models_with_layer+dropout+lr/"+str(i)+".pth")

          # translation = translate(source_text, model, beamsize=BEAMSIZE, onlybest=True)
          # h.saveText(translation, f"with_b{BEAMSIZE}_layernorm+dropout+lr_"+str(i)+".txt")
        for i in range(1,11):
          model = torch.load("/content/models_with_layer+dropout+lr.3/"+str(i)+".pth")
          translation = translate(source_text, model, beamsize=BEAMSIZE, onlybest=True)
          h.saveText(translation, f"with_b{BEAMSIZE}_layernorm+dropout+lr.3_"+str(i)+".txt")
