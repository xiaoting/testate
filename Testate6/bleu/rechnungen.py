import bleu
import wer
import per
import sys

REFERENCE = "multi30k.dev.en.txt"
HYPOTHESIS = "rnn_v2_epoch8_b5_best.txt"

# Input-Textdateien in passendes Arrayformat umwandeln, damit die Funktionen damit arbeiten koennen
ref_arr = list()
hyp_arr = list()

referenz = open(REFERENCE)
for line in referenz:
    ref_arr.append(line.split())
referenz.close()

hypothese = open(HYPOTHESIS)
for line in hypothese:
    hyp_arr.append(line.split())
hypothese.close()

pairs = [[[""] for i in range(2)] for j in range(max(len(ref_arr), len(hyp_arr)))]
for i in range(len(ref_arr)):
    pairs[i][0] = ref_arr[i]

for i in range(len(hyp_arr)):
    pairs[i][1] = hyp_arr[i]

# # Berechnung von WER (auf Korpuslevel)
# w = wer.wer_korpus(pairs)
# print("WER:")
# print(w)

# # Berechnung von PER (auf Korpuslevel)
# p = per.per_korpus(pairs)
# print("PER:")
# print(p)

# Berechnung von BLEU
b = bleu.bleu4(pairs)
print("BLEU:")
print(b)