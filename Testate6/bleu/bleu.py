import math
import sys

# Funktion, die die Uebereinstimmungen in Hypothese und pReferenz fuer gegebenes n zaehlt
def ngrammatches(ref, hyp, n):
    used = list()
    matches = 0
    
    # n-grams in der Hypothese
    for i in range( len(hyp)-(n-1) ):
        
        # Skip n-gram if it is already been used
        for u in used:    
            if equal(hyp[i:i+n], hyp[u:u+n]):
                continue

        # n-grams in der Referenz
        for j in range( len(ref)-(n-1) ):
            ismatch = False
            
            # Compare each word in n-grams
            if equal(hyp[i:i+n], ref[j:j+n]):
                ismatch = True
                matches += 1
            
        # If the n-grams match, save the used n-gram
        if ismatch:
            used.append(i)
                
    return matches

# Hilfsfunktion, die zwei gegebene Arrays vergleicht
def equal(arr1, arr2):
    if len(arr1) != len(arr2):
        return False
    
    res = True
    for i in range(len(arr1)):
        if not (arr1[i] == arr2[i]):
            res = False
            break

    return res


# Modifizierte n-gram Praezision
def modpn(pairs, n):                   # pairs hat L Reihen mit je zwei Elemente (welche auch Arrays sind)
    sum1 = 0
    sum2 = 0

    # Zaehler berechnen: Summe der Minima der Anzahlen der verschiedenen n-grams in Hypothesen und Referenzen
    for l in range(len(pairs)):
        
        # Liste von einzigartigen n-grams in der aktuellen Hypothese erstellen
        ngrams = list()
        for i in range(len(pairs[l][1])-(n-1)):
            ngram = pairs[l][1][i:i+n]

            if ngram not in ngrams:
                ngrams.append(ngram)

        # For every ngram, increment sum1 by minimum of ngrams in either reference or hypothesis
        for ngram in ngrams:
            ngrams_in_hyp = ngramcounter(ngram, pairs[l][1])

            sum1 += min( ngramcounter(ngram, pairs[l][0]) , ngrams_in_hyp )

            sum2 += ngrams_in_hyp


        # used = list()
        # for i in range(len(pairs[l][1])-(n-1)):            
            
        #     # Skip n-gram if it is already been used
        #     for u in used:    
        #         if equal(pairs[l][1][i:i+n], pairs[l][1][u:u+n]):
        #             continue

        #     # Increment sum by minimum of ngrams in hypothesis or reference
        #     sum1 += min( ngramcounter(pairs[l][1][i:i+n],pairs[l][0]) , ngramcounter(pairs[l][1][i:i+n],pairs[l][1]) )
        #     used.append(i)


    # # Nenner berechnen: Summe der Anzahlen an unterschiedlichen n-grams in den Hypothesen
    # sum2 = 0

    # for l in range(len(pairs)):
    #     ngrams = 0
    #     for i in range(len(pairs[l][1])-(n-1)):
    #         isduplicate = False
    #         for j in range(i):
    #             if equal(pairs[l][1][i:i+n], pairs[l][1][j:j+n]):
    #                 isduplicate = True
    #         if not isduplicate:
    #             ngrams += 1
    #     sum2 += ngrams

    # Calculate Pn
    Pn = sum1 / sum2
    return Pn

# Hilfsfunktion, die Anzahl n-grams zaehlt
def ngramcounter(ngram, phrase):
    counter = 0
    n = len(ngram)

    for i in range(len(phrase)-(n-1)):
        if equal(ngram, phrase[i:i+n]):
            counter += 1
    
    return counter

# Brevity Penalty
def bp(c,r):
    if c > r:
        return 1
    else:
        return math.exp(1-r/c)

# BLEU
def bleu(pairs,n):
    # Teilsumme
    sum = 0
    for i in range(1,n+1):
        sum += (1/n) * math.log(modpn(pairs,i))

    # Gesamtlaenge aller Hypothesen
    c = 0
    for l in range(len(pairs)):
        c += len(pairs[l][1])

    # Gesamtlaenge aller Referenzen
    r = 0
    for l in range(len(pairs)):
        r += len(pairs[l][0])

    # BLEU
    bleu = bp(c,r) * math.exp(sum)
    return bleu * 100

# BLEU fuer n=4
def bleu4(pairs):
    return bleu(pairs,4) 