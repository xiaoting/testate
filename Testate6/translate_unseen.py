import bpe
import helper as h
import rnn_with_layer_dropout_lr as rnn
import pickle
import torch

## PARAMETERS
SOURCE_FILE = "multi30k.de"

MODEL_PATH = "/media/joyce/LENOVO/DATA/Tonja/models/layer_norm/rnn_epoch7"

BEAM_SIZE = 5

## TRANSLATION
source_text = h.extractText(SOURCE_FILE)

# Apply BPE
union_ops_de = pickle.load(open("union_ops_de.text", "rb"))
source_text_bpe = bpe.bpe_apply(source_text, union_ops_de)

# Load model
model = torch.load(MODEL_PATH)

# Translate text
translation = rnn.translate(source_text_bpe, model, beamsize=BEAM_SIZE, onlybest=True)
h.saveText(translation, f"Translation.txt")