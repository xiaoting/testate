import batches
from Dictionary import Dictionary

# Open input files
source = open("multi30k.de")
source_text = source.read()
source.close()

target = open("multi30k.en")
target_text = target.read()
target.close()

# Set parameters
startline = 1100
endline = 1200
window = 2
asString = False

# Generate dictionaries
dict_de = Dictionary()
dict_de.generate(source_text)
dict_en = Dictionary()
dict_en.generate(target_text)

# Generate batches
batches = batches.makeBatches200(source_text, target_text, startline, endline, window, dict_de, dict_en, asString)

# Write batches to output file
output = open("batches", "a")

for batch in batches:
    for n in range(200):

        tabs0 = ""
        for i in range( 32 - len(str(batch[0][n])) ):
            tabs0 += " "

        tabs1 = ""
        for i in range( 16 - len(str(batch[1][n])) ):
            tabs1 += " "

        string = "{0}{1}{2}{3}{4}"
        line = string.format(batch[0][n], tabs0, batch[1][n], tabs1, batch[2][n])
        output.write( line )
        output.write("\n")

    output.write("\n")

output.close()