import sys
import bpe

# Input: file name and number of contraction operations are given as arguments
input_file = sys.argv[1]
#input_file = "multi30k.de"
ops_num = sys.argv[2]
#ops_num = 10

# Open input file and read text
file = open(input_file)
text = file.read()
file.close()

# Learn BPE and apply it to text
union_ops = bpe.bpe_learn(text, ops_num)
bpe_text = bpe.bpe_apply(text, union_ops)

# Write output text to output file
output = open("output_file", "x")
output.write(bpe_text)
output.close()