import sys
import re

# Function that calculates a list of BPE union operations with a training text and given number of unions
def bpe_learn(text, ops_num):

    # Dictionary of words and word frequency
    frequency = {}

    words = text.split()

    for word in words:
        if frequency.get(word) == None:
            count = 0
        else:
            count = frequency.get(word)
        frequency[word] = count + 1

    # Split words into single characters and append end-of-word-symbol
    tokens = list()

    for word in frequency:
        chars = list()
        for c in word:
            chars.append(c)
#        chars[-1] = chars[-1] + "@@"
        tokens.append(chars)

    # Initialization of output list
    union_ops = list()

    # Repeat following code ops_num times
    for n in range(int(ops_num)):

        # Count token pairs
        pairs = {}
        for word in tokens:
            for i in range(len(word)-1):
                if word[i] + word[i+1] not in pairs:
                    pairs[word[i]+word[i+1]] = frequency.get( "".join(word) )    
                else:
                    pairs[word[i]+word[i+1]] += frequency.get( "".join(word) )  

        # Find most frequent token pair
        if not len(pairs) == 0:
            maximum = max(pairs.values())
        else:
            return union_ops                     # in case all words have been reconstructed

        for pair in pairs:
            if pairs[pair] == maximum:
                key_to_be_united = pair
                break

        # Unite most frequent token pair
        half1 = ""
        half2 = ""
        
        for word in tokens:
            l = len(word)-1
            i = 0
            while i < l:
                if word[i] + word[i+1] == key_to_be_united:
                    half1 = word[i]                             # TODO ganz doof dass das bei jd schleifendurchlauf geupdatet wird
                    half2 = word[i+1]                           # ja lol half1 und half2 in dem union_ops array werden nicht gebraucht
                    word[i] = word[i] + word[i+1]
                    word.pop(i+1)
                    l -= 1
                i += 1               

        # Add union operation of current most frequent token pair to output list
        op = [key_to_be_united, half1, half2]
        union_ops.append(op)

    # Output: list of union operations
    return union_ops


# Function that applies a given list of BPE union operations on a given text
def bpe_apply(text, union_ops):     # TODO in-word-character?
    
    # Splitting words into lists and modifying last letter and putting these lists into big array
    # Lines is list, line is list of words, word is list of tokens (chars in the beginning)
    lines = text.splitlines()

#    for line in lines:
#        split_line = line.split()
#        line = split_line
#        for word in line:
#            chars = list()
#            for c in word:
#                chars.append(c)
#            word = chars
            
    for i in range(len(lines)):
        lines[i] = lines[i].split()
        for j in range(len(lines[i])):
            chars = list()
            for c in lines[i][j]:
                chars.append(c)
            lines[i][j] = chars

    # Union operations according to given BPE operations
    for op in union_ops:
        for line in lines:
            for word in line:
                l = len(word)-1
                i = 0
                while i < l:                            
                    if word[i] + word[i+1] == op[0]:
                        word[i] = op[0]
                        word.pop(i+1)
                        l -= 1
                    i += 1

    # Add in-word-character "@@" at end of tokens that end in word (all except last token in word)
    for line in lines:
        for word in line:
            for i in range(len(word)-1):
                word[i] = word[i] + "@@"

    # Make output text from list of list of list of tokens
    for i in range(len(lines)):
        for j in range(len(lines[i])):
            lines[i][j] = " ".join(lines[i][j])
        lines[i] = " ".join(lines[i])
    bpe_text = "\n".join(lines)
    
#    for line in lines:
#        for word in line:
#            word = " ".join(word)
#        line = " ".join(line)
#    bpe_text = "\n".join(lines)

    return bpe_text


# Function that undos the BPE of a text 
def bpe_undo(text_in):

    # Split text to line array and lines to word arrays and words to token arrays and remove "@@"
    text = text_in.splitlines()
    for line in range(len(text)):
        
        temp = text[line].split()
        normal_line = list()
        word = ""

        for token in temp:
            if token.endswith("@@"):
                word += token.replace("@@","")
            if not token.endswith("@@"):
                word += token
                normal_line.append(word)
                word = ""

        text[line] = normal_line

    # Rebuild text from table
    for line in range(len(text)):
        text[line] = " ".join(text[line])
    normal_text = "\n".join(text)

    return normal_text
