from Dictionary import Dictionary

# Function to generate batch, assumption: between firstword to lastword are 200 words
def generateBatch(source_text, target_text, firstline, lastline, firstword, lastword, window, dict_de, dict_en):
    
    # Source and target text split up in lines
    lines_src = source_text.splitlines()
    lines_tgt = target_text.splitlines()

    batchlines_src = []
    batchlines_tgt = []

    # Buffer beginning of sentence
    start_buffer = list()
    for j in range(window):
        start_buffer.append("<s>")

    # Buffer end of sentence (only for source)
    end_buffer = list()
    for j in range(window + 1):
        end_buffer.append("</s>")

    # Append buffer and sentences (split up into word arrays)
    for i in range(lastline - firstline + 1):
        batchlines_src.append( start_buffer + lines_src[firstline + i].split() + end_buffer )
        batchlines_tgt.append( start_buffer + lines_tgt[firstline + i].split() + ["</s>"] )


# Generate S (source windows)
    s = list()

    # Append source window (list) to S for each word in the first line of batchlines_tgt
    for word in range(firstword + window, len(batchlines_tgt[0])):
        src_window = list()
        b = alignment(source_text, firstline, word-window)

        for j in range(2*window + 1):
            index = dict_de.getIndex( batchlines_src[0][b+j] )
            src_window.append(index)

        s.append(src_window)

    # Append source window (list) to S for each word in all lines between first and last line of batchlines_tgt
    for line in range(1, len(batchlines_src)-1):

        for word in range(window, len(batchlines_tgt[line])):
            src_window = list()
            b = alignment(source_text, firstline+line, word-window)

            for j in range(2*window + 1):
                index = dict_de.getIndex( batchlines_src[line][b+j] )
                src_window.append(index)

            s.append(src_window)

    # Append source window (list) to S for each word in last line of batchlines_tgt
    last = len(batchlines_src) - 1
    for word in range(window, lastword + window + 1):
        src_window = list()
        b = alignment(source_text, lastline, word-window)

        for j in range(2*window + 1):
            index = dict_de.getIndex( batchlines_src[last][b+j] )
            src_window.append(index)
        
        s.append(src_window)


# Generate T (target windows)
    t = list()

    # Append target window (list) to T for each word in the first line of batchlines_tgt
    for word in range(firstword + window, len(batchlines_tgt[0])):
        tgt_window = list()

        for w in range(window):
            index = dict_en.getIndex( batchlines_tgt[0][word-(window-w)] )
            tgt_window.append(index)

        t.append(tgt_window)

    # Append target window (list) to T for each word in all lines between first and last line of batchlines_tgt
    for line in range(1, len(batchlines_tgt)-1):

        for word in range(window, len(batchlines_tgt[line])):
            tgt_window = list()

            for w in range(window):
                index = dict_en.getIndex( batchlines_tgt[line][word-(window-w)] )
                tgt_window.append(index)

            t.append(tgt_window)

    # Append target window (list) to T for each word in last line of batchlines_tgt
    last = len(batchlines_tgt) - 1
    for word in range(window, lastword + window + 1):
        tgt_window = list()

        for w in range(window):
            index = dict_en.getIndex( batchlines_tgt[last][word-(window-w)] )
            tgt_window.append(index)

        t.append(tgt_window)


# Generate L (target labels):
    l = list()

    # Append needed words from first line (from index firstword)
    for word in range(firstword + window, len(batchlines_tgt[0])):
        l.append( dict_en.getIndex(batchlines_tgt[0][word]) )

    # Append words from all lines in between startline and endline
    for line in range(1, len(batchlines_tgt)-1):
        for word in range(window, len(batchlines_tgt[line])):
            l.append( dict_en.getIndex( batchlines_tgt[line][word] ) )

    # Append needed words from last line (to index lastword)
    for word in range(window, lastword + window + 1):
        l.append( dict_en.getIndex(batchlines_tgt[len(batchlines_tgt)-1][word]) )

# BATCH
    return (s, t, l)


# Function to generate batch, assumption: between firstword to lastword are 200 words
def generateBatchStr(source_text, target_text, firstline, lastline, firstword, lastword, window):
    
    # Source and target text split up in lines
    lines_src = source_text.splitlines()
    lines_tgt = target_text.splitlines()

    batchlines_src = []
    batchlines_tgt = []

    # Buffer beginning of sentence
    start_buffer = list()
    for j in range(window):
        start_buffer.append("<s>")

    # Buffer end of sentence (only for source)
    end_buffer = list()
    for j in range(window + 1):
        end_buffer.append("</s>")

    # Append buffer and sentences (split up into word arrays)
    for i in range(lastline - firstline + 1):
        batchlines_src.append( start_buffer + lines_src[firstline + i].split() + end_buffer )
        batchlines_tgt.append( start_buffer + lines_tgt[firstline + i].split() + ["</s>"] )


# Generate S (source windows)
    s = list()

    # Append source window (list) to S for each word in the first line of batchlines_tgt
    for word in range(firstword + window, len(batchlines_tgt[0])):
        src_window = list()
        b = alignment(source_text, firstline, word-window)

        for j in range(2*window + 1):
            index = batchlines_src[0][b+j]
            src_window.append(index)

        s.append(src_window)

    # Append source window (list) to S for each word in all lines between first and last line of batchlines_tgt
    for line in range(1, len(batchlines_src)-1):

        for word in range(window, len(batchlines_tgt[line])):
            src_window = list()
            b = alignment(source_text, firstline+line, word-window)

            for j in range(2*window + 1):
                index = batchlines_src[line][b+j]
                src_window.append(index)

            s.append(src_window)

    # Append source window (list) to S for each word in last line of batchlines_tgt
    last = len(batchlines_src) - 1
    for word in range(window, lastword + window + 1):
        src_window = list()
        b = alignment(source_text, lastline, word-window)

        for j in range(2*window + 1):
            index = batchlines_src[last][b+j]
            src_window.append(index)
        
        s.append(src_window)


# Generate T (target windows)
    t = list()

    # Append target window (list) to T for each word in the first line of batchlines_tgt
    for word in range(firstword + window, len(batchlines_tgt[0])):
        tgt_window = list()

        for w in range(window):
            index = batchlines_tgt[0][word-(window-w)]
            tgt_window.append(index)

        t.append(tgt_window)

    # Append target window (list) to T for each word in all lines between first and last line of batchlines_tgt
    for line in range(1, len(batchlines_tgt)-1):

        for word in range(window, len(batchlines_tgt[line])):
            tgt_window = list()

            for w in range(window):
                index = batchlines_tgt[line][word-(window-w)]
                tgt_window.append(index)

            t.append(tgt_window)

    # Append target window (list) to T for each word in last line of batchlines_tgt
    last = len(batchlines_tgt) - 1
    for word in range(window, lastword + window + 1):
        tgt_window = list()

        for w in range(window):
            index = batchlines_tgt[last][word-(window-w)]
            tgt_window.append(index)

        t.append(tgt_window)


# Generate L (target labels):
    l = list()

    # Append needed words from first line (from index firstword)
    for word in range(firstword + window, len(batchlines_tgt[0])):
        l.append( batchlines_tgt[0][word] )

    # Append words from all lines in between startline and endline
    for line in range(1, len(batchlines_tgt)-1):
        for word in range(window, len(batchlines_tgt[line])):
            l.append( batchlines_tgt[line][word] )

    # Append needed words from last line (to index lastword)
    for word in range(window, lastword + window + 1):
        l.append( batchlines_tgt[len(batchlines_tgt)-1][word] )

# BATCH
    return (s, t, l)


# Alignment function
def alignment(source_text, line, index):
    src_lines = source_text.splitlines()
    for i in range(len(src_lines)):
        src_lines[i] = src_lines[i].split()

    if index < len(src_lines[line]) + 1:
        b = index
    else:
        b = len(src_lines[line])

    return b


# Generates list of batches of given size for given text segment
def makeBatches(source_text, target_text, startline, endline, window, dict_de, dict_en, asString, batchsize):
    batches = list()

    lines = target_text.splitlines()
    for line in range(len(lines)):
        lines[line] = lines[line].split()

    firstline = startline
    firstword = 0
    counter = 0

    for line in range(startline, endline):
        for word in range(len(lines[line]) + 1):        # plus 1 for the </s> which has its own line in batch
            counter += 1

            # Count until we have batchsize words
            if counter == batchsize:
                lastline = line
                lastword = word

                if asString:
                    batches.append(generateBatchStr(source_text, target_text, firstline, lastline, firstword, lastword, window))
                else:
                    batches.append(generateBatch(source_text, target_text, firstline, lastline, firstword, lastword, window, dict_de, dict_en))
                
                # Reset all relevant values so we can continue with the next batch
                counter = 0
                if not word == len(lines[line]) - 1:
                    firstline = line
                    firstword = word + 1
                else:
                    firstline = line + 1
                    firstword = 0

    return batches


# Generates list of batches of size 200 for given text segment
def makeBatches200(source_text, target_text, startline, endline, window, dict_de, dict_en, asString):
    return makeBatches(source_text, target_text, startline, endline, window, dict_de, dict_en, asString, 200)
