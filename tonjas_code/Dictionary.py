class Dictionary:
    def __init__(self):
        self.dict = list()
        self.dict.append("<s>")
        self.dict.append("</s>")
        self.dict.append("<UNK>")

    # Function to add string-index-pairs to vocabulary
    def add(self, string):
        self.dict.append(string)

    # Function to get index of string
    def getIndex(self, string):
        for i in range(len(self.dict)):
            if self.dict[i] == string:
                return i

    # Function to get string of index
    def getWord(self, index):
        return self.dict[index]

    # Function to generate vocabulary from text
    def generate(self, text):
        words = text.split()
        for word in words:
            if word not in self.dict:
                self.dict.append(word)

    # Function to apply vocabulary to new data set
    def apply(self, text):
        lines = text.splitlines()
        for line in range(len(lines)):
            lines[line] = lines[line].split()

        for line in lines:
            for word in line:
                if word not in self.dict:
                    word = "<UNK>"

        # Rebuild text from table
        for line in lines:
            line = " ".join(line)
        text_out = "\n".join(lines)

        return text_out
