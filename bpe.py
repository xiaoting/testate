import sys
import re
import preprocessing as pre
import collections

# Function that calculates a list of BPE union operations with a training text and given number of unions
def bpe_learn(text, ops_num):

    words = text.split()
    frequency = collections.defaultdict(int)
    for word in words:
        frequency[word]+=1

    # Split words into single characters and append end-of-word-symbol
    tokens = list()
    for word in frequency:
        chars = list()
        for c in word:
            chars.append(c)
        chars[-1] = chars[-1] + "</w>"
        tokens.append(chars)

    # Initialization of output list
    union_ops = list()

    # Repeat following code ops_num times
    for _ in range(int(ops_num)):
        # Count token pairs
        pairs = collections.defaultdict(int)
        for word in tokens:
            for i in range(len(word)-1):
                pairs[word[i]+word[i+1]] += frequency.get( ("".join(word)).replace("</w>","") )  
        # Find most frequent token pair
        if not len(pairs) == 0:
            key_to_be_united = max(pairs, key=pairs.get)
        # In case all words have been reconstructed just return the current list of union operations
        else:
            return union_ops                     

        # Unite most frequent token pair        
        for word in tokens:
            unitePair(word, key_to_be_united)         
        # Add union operation of current most frequent token pair to output list
        union_ops.append(key_to_be_united)
    # Output: list of union operations
    return union_ops


# Function that applies a given list of BPE union operations on a given text
def bpe_apply(text, union_ops): 
    
    # Splitting words into lists and modifying last letter and putting these lists into big array
    lines = splitText(text)

    # Union operations according to given BPE operations
    for op in union_ops:
        for line in lines:
            for word in line:
                unitePair(word, op)

    # Add in-word-character "@@" at end of tokens that end in word and remove "</w>" (end-of-word-character)
    for line in lines:
        for word in line:
            for i in range(len(word)):
                if not i == len(word)-1:
                    word[i] = word[i] + "@@"
                else:
                    word[i] = word[i].replace("</w>", "")

    # Make output text from list of list of list of tokens
    bpe_text = rebuildText(lines)

    return bpe_text

# Function that undos the BPE of a text 
def bpe_undo(text_in):
    normal_text = text_in.replace("@@ ", "")
    return normal_text

# Function unites token pairs in given array (word) that equal given string (pair)
def unitePair(word, tokenpair):
        l = len(word)-1
        i = 0
        while i < l:                            
            if word[i] + word[i+1] == tokenpair:
                word[i] = tokenpair
                word.pop(i+1)
                l -= 1
            i += 1

# Function splits text into array of lines, which are arrays of words, which are arrays of chars
def splitText(text):
    lines = text.splitlines()
            
    for i in range(len(lines)):
        lines[i] = lines[i].split()
        for j in range(len(lines[i])):
            chars = list()
            for c in lines[i][j]:
                chars.append(c)
            # End-of-word-symbol </w> is added to last char in word
            chars[-1] = chars[-1] + "</w>"

            lines[i][j] = chars
    
    return lines

# Function rebuilds text (string) from 3d array of tokens
def rebuildText(lines):
    for i in range(len(lines)):
        for j in range(len(lines[i])):
            lines[i][j] = " ".join(lines[i][j])
        lines[i] = " ".join(lines[i])
    text = "\n".join(lines)
    return text


if __name__ == "__main__":

    # Input: file name and number of contraction operations are given as arguments
    # input_file = sys.argv[1]
    input_file = r"Data/multi30k.en"

    # ops_num = sys.argv[2]
    ops_num = 7000

    # Open input file and read text
    # text = pre.read(input_file)
    file = open(input_file)
    text = file.read()
    file.close()

    # Learn BPE and apply it to text
    union_ops = bpe_learn(text, ops_num)
    bpe_text = bpe_apply(text, union_ops)

    # Write output text to output file
    output = open("built_out", "w")
    output.write(bpe_text)
    output.close()

    # # input_file = sys.argv[1]
    # input_file = "built_out"
    # text = pre.read(input_file)
    # text_out = bpe_undo(text)

    # output = open("rebuilt_out", "w")
    # output.write(text_out)
    # output.close()