
# Helper function to extract text from file
def extractText(filename):
    file = open(filename)
    text = file.read()
    file.close()
    return text 

# Write text to output file
def saveText(text, out_filename):
    output = open(out_filename, "w")
    output.write(text)
    output.close()

# Split text into line array of word arrays 
def tableText2D(text):
    text = text.splitlines()
    for line in range(len(text)):
        text[line] = text[line].split()
    return text

# Rebuild text from table of words (2D)
def rebuildText2D(table):
    for line in range(len(table)):
        table[line] = " ".join(table[line])
    text = "\n".join(table)
    return text

# Rebuild text from table of characters/tokens (3D)
def rebuildText3D(table):
    for line in range(len(table)):
        for word in range(len(table[line])):
            table[line][word] = "".join(table[line][word])
        table[line] = " ".join(table[line])
    text = "\n".join(table)
    return text