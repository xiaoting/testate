import sys
import re
import helper as h

# Function that calculates a list of BPE union operations with a training text and given number of unions
def bpe_learn(text, ops_num):

    # Dictionary of words and word frequency
    frequency = {}

    words = text.split()

    for word in words:
        if frequency.get(word) == None:
            count = 0
        else:
            count = frequency.get(word)
        frequency[word] = count + 1

    # Split words into single characters and append end-of-word-symbol
    tokens = list()

    for word in frequency:
        chars = list()
        for c in word:
            chars.append(c)
        chars[-1] = chars[-1] + "</w>"
        tokens.append(chars)

    # Initialization of output list
    union_ops = list()

    # Repeat following code ops_num times
    for _ in range(int(ops_num)):

        # Count token pairs
        pairs = {}
        for word in tokens:
            for i in range(len(word)-1):
                if word[i] + word[i+1] not in pairs:
                    pairs[word[i]+word[i+1]] = frequency.get( ("".join(word)).replace("</w>","") )    
                else:
                    pairs[word[i]+word[i+1]] += frequency.get( ("".join(word)).replace("</w>","") )  

        # Find most frequent token pair
        if not len(pairs) == 0:
            key_to_be_united = max(pairs, key=pairs.get)

        # In case all words have been reconstructed just return the current list of union operations
        else:
            return union_ops                     

        # Unite most frequent token pair        
        for word in tokens:
            unitePair(word, key_to_be_united)         

        # Add union operation of current most frequent token pair to output list
        union_ops.append(key_to_be_united)

    # Output: list of union operations
    return union_ops


# Function that applies a given list of BPE union operations on a given text
def bpe_apply(text, union_ops):     
    # bpe apply on vocab nested double for loop with break 
    
    # Splitting words into lists and modifying last letter and putting these lists into big array
    lines = splitText(text)

    # Union operations according to given BPE operations
    for op in union_ops:
        for line in lines:
            for word in line:
                unitePair(word, op)

    # Add in-word-character "@@" at end of tokens that end in word and remove "</w>" (end-of-word-character)
    for line in lines:
        for word in line:
            for i in range(len(word)):
                if not i == len(word)-1:
                    word[i] = word[i] + "@@"
                else:
                    word[i] = word[i].replace("</w>", "")

    # Make output text from list of list of list of tokens
    bpe_text = h.rebuildText3D(lines)

    return bpe_text

# Function that undos the BPE of a text 
def bpe_undo(text_in):
    normal_text = text_in.replace("@@ ", "")
    return normal_text

# Function unites token pairs in given array (word) that equal given string (pair)
def unitePair(word, tokenpair):
        l = len(word)-1
        i = 0
        while i < l:                            
            if word[i] + word[i+1] == tokenpair:
                word[i] = tokenpair
                word.pop(i+1)
                l -= 1
            i += 1

# Function splits text into array of lines, which are arrays of words, which are arrays of chars
def splitText(text):
    lines = text.splitlines()
            
    for i in range(len(lines)):
        lines[i] = lines[i].split()
        for j in range(len(lines[i])):
            chars = list()
            for c in lines[i][j]:
                chars.append(c)
            
            # End-of-word-symbol </w> is added to last char in word
            chars[-1] = chars[-1] + "</w>"

            lines[i][j] = chars
    
    return lines
