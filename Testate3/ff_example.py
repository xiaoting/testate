from Dictionary import Dictionary
import batches
import torch
from torch.utils.data import Dataset
import torch.nn as nn
import torch.nn.functional as F
from torch.optim import Adam
import math
import helper as h

## PARAMETERS ------------------------------------------------------------

# Batch parameters
window = 1
batch_size = 200
source_file = "output_de_7k"
target_file = "output_en_7k"

# Neural network parameters
device = "cpu"
# device = "cuda" if torch.cuda.is_available() else "cpu"
emb_dim = 100

# Training parameters
losstype = nn.CrossEntropyLoss()
EPOCHS = 10
learning_rate = 0.001
lr_adaption = False
# saving_freq = 
path = "/home/joyce/SPP/models"

# Dictionary sources
dict_source = "output_de_7k"
dict_target = "output_en_7k"


## GLOBALS ---------------------------------------------------------------

# Generate dictionaries
dict_de = Dictionary(dict_source)
dict_en = Dictionary(dict_target)


## NEURAL NETWORK ARCHITECTURE -------------------------------------------

# Embedding module
class WordEmbedding(nn.Module):
    def __init__(self, dict_size, emb_dim):
        super(WordEmbedding, self).__init__()
        self.embedding = nn.Embedding(dict_size, emb_dim)

    def forward(self, tensor):
        return self.embedding(tensor)

# "Actual" network module
class Network(nn.Module):

    def __init__(self):
        super(Network, self).__init__()

        self.source_stack = nn.Sequential(
            # Embedding layer
            nn.Embedding(dict_de.len(), emb_dim),
            
            # Fully connected layer
            nn.Linear(100, 150),
            nn.ReLU(),
            # nn.BatchNorm1d()
        )

        self.target_stack = nn.Sequential(
            # Embedding layer
            nn.Embedding(dict_en.len(), emb_dim),
            
            # Fully connected layer
            nn.Linear(100, 150),
            nn.ReLU(),
            # nn.BatchNorm1d()
        )
    
        self.linear_relu_stack = nn.Sequential(
            
            # Fully connected layer 1
            nn.Linear(600, 600),
            nn.ReLU(),
            # nn.BatchNorm1d(),   # TODO 1d??

            # Fully connected layer 2
            nn.Linear(600, dict_en.len())
            # nn.BatchNorm1d(),

        )

        # Softmax layer
        self.emb_back = nn.Linear(emb_dim, dict_en.len())
        self.softmax = nn.Softmax(dim=1)


    def forward(self, s, t):
        # s = torch.transpose(s, 1, 2)
        # t = torch.transpose(t, 1, 2)
        
        s = self.source_stack(s)
        t = self.target_stack(t)

        x = torch.cat((s, t), 1)
        x = torch.reshape(x, (len(s), 1, 600))
        x = self.linear_relu_stack(x)

        x = x.view(len(x), dict_en.len())
        # x = self.emb_back(x)
        # x = self.softmax(x)

        return x


## TRAINING THE MODEL ----------------------------------------------------
def train(model, s, t, l, optimizer, losstype):
    
    # Reset model gradients
    model.zero_grad()
    
    # Calculate output
    output = model(s, t)

    # l = l.view(200,100,1)
    # output = output.view(batch_size, emb_dim)

    # Calculate loss and update weights
    loss = losstype(output, l)
    loss.backward(retain_graph=True)
    optimizer.step()
    # optimizer.zero_grad()

    # Manually apply softmax in order to determine predictions
    softmax = nn.Softmax(dim=1)
    output_softmaxed = softmax(output)
    output_sftmxd_list = output_softmaxed.tolist()
    predictions = list()
    for i in range(len(output_sftmxd_list)):
        index_of_max = output_sftmxd_list[i].index(max(output_sftmxd_list[i]))
        predictions.append(index_of_max)

    return loss, predictions

# Use model to evaluate
def evaluate(model, s, t):
    
    # Calculate output
    probs = model(s, t)

    # Manually apply softmax in order to determine predictions
    softmax = nn.Softmax(dim=1)
    output_softmaxed = softmax(probs)
    output_sftmxd_list = output_softmaxed.tolist()
    predictions = list()
    for i in range(len(output_sftmxd_list)):
        index_of_max = output_sftmxd_list[i].index(max(output_sftmxd_list[i]))          # mit topk
        predictions.append(index_of_max)

    return output_sftmxd_list, predictions


# Model parameter print function
def printModel(model):
    for param1, param2 in zip(model.parameters(), model.named_parameters()):
        print( 'Parameter: {} has shape: {}'.format(param2[0], param1.shape) )
        print('\n')


# Main
if __name__ == "__main__":

    print("Device: " + device)

    # Preparations
    source = h.extractText(source_file)
    target = h.extractText(target_file)

    # Initialize the DataSet
    batches = batches.makeBatches(source, target, 0, 1000, window, dict_de, dict_en, False, batch_size)
    # reset to hyperparameters

    s_list = [batch[0] for batch in batches]
    t_list = [batch[1] for batch in batches]
    l_list = [batch[2] for batch in batches]

    s_tensor = torch.tensor(s_list)
    t_tensor = torch.tensor(t_list)
    l_tensor = torch.tensor(l_list)

    # Embedding
    # emb_de = WordEmbedding(dict_de.len(), emb_dim)
    # emb_en = WordEmbedding(dict_en.len(), emb_dim)
    # label_emb = LabelEmbedding(dict_en.len(), emb_dim)

    # s_emb = emb_de(s_tensor)
    # t_emb = emb_en(t_tensor)
    # l_emb = label_emb(l_tensor)

    # Sets requires_grad attribute to True
    s_tensor = s_tensor.float()
    t_tensor = t_tensor.float()

    s_tensor.requires_grad = True
    t_tensor.requires_grad = True

    s_tensor = s_tensor.long()
    t_tensor = t_tensor.long()


    # src_windows = torch.from_numpy(batches[:,0])
    # tgt_windows = torch.from_numpy(batches[:,1])
    # labels = torch.from_numpy(batches[:,2])

    # Load the Dataset
    # data_train = DataLoader(dataset = data, batch_size = 1, shuffle = False)

    # torch.manual_seed(0.5)

    # Initialize a instance of the network
    net = Network()
    # for param in net.parameters():
    #    print(param)

    printModel(net)

    optm = Adam(net.parameters(), lr = learning_rate)
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optm, 'min', 0.5, 5, verbose=True,)
    # last_epoch_loss = 0

    # Training loop
    for epoch in range(EPOCHS):
        epoch_acc = 0
        epoch_loss = 0
        correct = 0

        for s, t, l in zip(s_tensor, t_tensor, l_tensor):

            loss, predictions = train(net, s, t, l, optm, losstype)
           
            for i, label in enumerate(l):
                if predictions[i] == label:
                    correct += 1
            # acc = correct/len(l)

            epoch_loss += loss
            batch_ppl = math.exp(loss)
            # print('Batch Loss : {}'.format(loss))
            # print('Batch Perplexity : {}'.format(batch_ppl))

        epoch_acc = correct/(batch_size*len(batches))
        epoch_ppl = math.exp(epoch_loss)
        
        print('Epoch {} Accuracy : {}'.format(epoch+1, epoch_acc*100))
        print('Epoch {} Loss : {}'.format((epoch+1),epoch_loss))
        print('Epoch {} Perplexity : {}'.format(epoch+1, epoch_ppl))

        # Save net
        torch.save(net, path)

        # Check if performance is stagnating and maybe adapt learning rate
        scheduler.step(round(epoch_loss.item()))

        # performance_stagnating = False
        # counter = 0

        # if abs(last_epoch_loss - epoch_loss) < 0.000001*epoch_loss:
        #     counter += 1
        #     if counter > 5:
        #         performance_stagnating = True

        # if lr_adaption and performance_stagnating:
        #     learning_rate /= 2
        #     torch.optim.lr_scheduler.ReduceLROnPlateau(optm, 'min', 0.5, 5)
