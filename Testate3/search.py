from gettext import translation
import torch
import ff_example as ff
import network as n
from ff_example import Network
from Dictionary import Dictionary
import batches
import helper as h
# import bleu
import bpe


## PARAMETERS ---------------------------------------------------------

dict_source = "output_de_7k"
dict_target = "output_en_7k"

window = 1
batchsize = 1

beamsize = 20
searchtype = "greedy"
inputfile = "multi30k.dev.de"
MAX_SENTENCE_LENGTH = 40

MODEL_PATH = "/home/joyce/SPP/models"


## GLOBALS ------------------------------------------------------------

# Generate dictionaries
source = h.extractText(dict_source)
dict_de = Dictionary()
dict_de.generate(source)

target = h.extractText(dict_target)
dict_en = Dictionary()
dict_en.generate(target)


## FUNCTIONS ----------------------------------------------------------

# Score calculation
#   - source satz und target satz gegeben
#   - tokenweise betrachten
#       - wahrscheinlichkeitsverteilung für token berechnen
#       - für das target satz token gucken, welcher wert in der wahrscheinlichkeitsverteilung steht
#   - alle tokenwahrscheinlichkeiten multiplizieren
# - für alle satzpaare ausdrucken 
def calculateScores(source_text, target_text, model):
    source = h.tableText2D(source_text)
    target = h.tableText2D(target_text)

    # Make source windows for all words in text
    s = batches.makeSrcWindows(source_text, window, dict_de)
    # s_tensor = torch.tensor(s)

    # Sentencewise
    probabilities = list()
    for line in range(len(target)):
    # for s_sentence, t_sentence in zip(source, target):
        
        # Target window that is dynamically extended while going through sentence
        tgt_window = list()
        for i in range(window):
            tgt_window.append(dict_en.getIndex("<s>"))

        # Tokenwise
        sentence_prob = 0
        # sentence_prob = 1
        for word in range(len(target[line])):
            
            # Calculate probability distributions for token in s_sentence
            if word < len(s[line]):                                         # alignment
                src_window = torch.tensor([s[line][word]])              
            tgt_tensor = torch.tensor([tgt_window])
            prob_distr, prediction = n.evaluate(model, src_window, tgt_tensor)

            # Look up index of target token
            index = dict_en.getIndex(target[line][word])
            
            # Look up probability of that index
            prob = prob_distr[0][index]
            sentence_prob += prob
            # sentence_prob *= prob

            # Adjust target window for next token
            tgt_window.append(prediction[0])
            tgt_window.pop(0)
    
        sentence_prob = sentence_prob/len(target[line])

        print("Sentence pair {} has score: {}\n".format(line, sentence_prob ))
        probabilities.append(sentence_prob)

    # output = open("output_scores", "a")
    # for i in len(probabilities):
    #     output.write("Sentence pair {} has score: {}\n".format(i, probabilities[i]))
    
    return sum(probabilities)/len(probabilities)

# Model benutzen (aber nicht trainieren)
# - target inputs müssen on the fly gespeichert und nutzbar werden
# - wie regeln wir das, weil unser Modell ja batchweise berechnet?

# Search
# - load trained model:     model.load(...)
# - greedy search & beam search
# - parameter outputtype: best translation or list of n best translations (greedy can't do n best)
# - optional step: remove bpe

def translate(text, model, searchtype, beamsize, onlybest, post):

    # Make source windows for all words in text
    s = batches.makeSrcWindows(text, window, dict_de)
    # s_tensor = torch.tensor(s)

    # According to search type
    if searchtype == "greedy":
        if beamsize > 1:
            print("Greedy search cannot return n best list.")
            return
        else:
            encoded_text = greedySearch(model, s)
    elif searchtype == "beam":
        encoded_text = beamSearch(model, s, beamsize)
        encoded_text = make2D(encoded_text)
    else:
        print("Search type does not exist.")
        return

    # Decode text as in replace indices with according words
    decoded_text = list()
    for i in range(len(encoded_text)):
        decoded_text.append([])
        for j in range(len(encoded_text[i])):
            word = dict_en.getWord(encoded_text[i][j])
            decoded_text[i].append(word)
    
    # decoded_text = [x for x in decoded_text if x != "<s>"]
    # decoded_text = [x for x in decoded_text if x != "</s>"]
        
    # Rebuild text from table
    translation = h.rebuildText2D(decoded_text)

    # Remove <s> and </s> symbols
    translation = translation.replace("<s> ", "")
    translation = translation.replace("<s>", "")
    translation = translation.replace(" </s>", "")

    # Remove BPE if postprocessing is asked for
    if post:
        translation = bpe.bpe_undo(translation)

    # Only give back best translation
    if onlybest:
        lines = translation.splitlines()
        best = lines[::beamsize+1]
        translation = "\n".join(best)

    return translation

# Helper for beam search result to put all sentences in one array and add empty lines
def make2D(text):
    temp = list()
    for line in text:
        for hyp in line:
            temp.append(hyp)
        temp.append([dict_en.getIndex("<s>")])
    return temp

# Greedy Search function
# - sentencewise:
#   - dynamic target window adjusting
#   - evaluate for every token and take max
#   - save max index
def greedySearch(model, s):

    encoded_text = list()

    # For each sentence
    for line in s:

        # Initialize target window that is dynamically extended while going through sentence
        start_buffer = [dict_en.getIndex("<s>")]*window
        t = start_buffer

        I_max = MAX_SENTENCE_LENGTH
        e_hat = list()
        e_hat.append( dict_en.getIndex("<s>") )           # bei windowsize > 1 brauchen wir mehr <s> oder?

        i = 0

        while e_hat[-1] != dict_en.getIndex("</s>") and i <= I_max:
            i += 1

            # Calculate predicted index
            if (i-1) < len(line):                                         # alignment
                s_tensor = torch.tensor([line[i-1]])
            t_tensor = torch.tensor([t])

            _, pred = ff.evaluate(model, s_tensor, t_tensor)
            e_hat.append(pred[0])

            # Adjust target window
            t.append(pred[0])
            t.pop(0)
        
        # Append translated sentence to final text table
        encoded_text.append(e_hat)

    return encoded_text

# Beam Search function
# - sentencewise:
#   - initialize t_tensor mit k target windows mit [<s>]*window
#   - initialize e_hat mit k-mal ["<s>"]
#   - initialize e_probs mit k-mal 1
#   - tokenwise:
#     - for all k hyps:
#       - calculate topks
#       - multiplicate probs with current prob
#     - look for topk new probs overall and update e_hat and e_probs (beam pruning)
def beamSearch(model, s, beamsize):
    
    encoded_texts = list()
    k = beamsize

    # For each sentence
    for line in s:
        
        # Initialize target windows that are dynamically extended while going through sentence
        start_buffer = [dict_en.getIndex("<s>")]*window
        
        # Initialize tensor that holds the k target windows
        t_tensor = torch.tensor([[start_buffer]]*k)

        I_max = MAX_SENTENCE_LENGTH

        # Initialize result tensor and tensor that holds probabilities for current top k hyps
        e_hat = list()
        for _ in range(k):
            e_hat.append([dict_en.getIndex("<s>")])
        e_probs = torch.tensor([1]*k).float()

        i = 0

        # First iteration
        i += 1
        s_tensor = torch.tensor([line[i-1]])
        test = t_tensor[0]
        probs, _ = ff.evaluate(model, s_tensor, t_tensor[0])
        values, indices = torch.topk(torch.tensor(probs), k)
        for j in range(k):
            e_hat[j].append(indices[0][j].item())
            e_probs[j] *= values[0][j].item()
            t_tensor[j] = torch.tensor(e_hat[j][-window:])

        # Following iterations
        while dict_en.getIndex("</s>") not in [ln[-1] for ln in e_hat] and i <= I_max:
            i += 1

            # Variable to save the k top-k 
            current = []

            if (i-1) < len(line):
                s_tensor = torch.tensor([line[i-1]])

            # Iterate through all of the k current hypotheses
            for j in range(k):

                # Calculate top k predicted indices for one current hypothesis
                probs, _ = ff.evaluate(model, s_tensor, t_tensor[j])
                values, indices = torch.topk( torch.tensor(probs) , k)

                # Multiplicate all values each with last probability
                values = values * e_probs[j]

                for v, idx in zip(values[0].tolist(), indices[0].tolist()):
                    current.append([j, v, idx])
                
            # Extract only the probability values from current and determine topk
            curr1 = list()
            for l in range(len(current)):
                curr1.append(current[l][1])
            _, indices = torch.topk(torch.tensor(curr1) , k)

            e_hat_new = list()
            e_probs_new = list()

            # Go through highest ranking hypotheses
            for idx in indices:

                # Look up corresponding token (index)
                token = current[idx][2]

                # Look up corresponding target history
                history = e_hat[current[idx][0]].copy()
                history.append(token)            

                e_hat_new.append(history)
                e_probs_new.append(current[idx][1])

            # Update e_hat and e_probs
            e_hat = e_hat_new
            e_probs = e_probs_new

            # Update target windows for next step
            for j in range(k):
                t_tensor[j] = torch.tensor( [e_hat[j][-window:]] )

        # Append translated sentences (!) to final text table
        encoded_texts.append(e_hat)

    return encoded_texts

# Early stopping
# - BLEU of every checkpoint of the model
# - anhand von MODEL_PATH
def earlyStopping(model_path):
    
    # Load model
    model = torch.load(MODEL_PATH)
    
    models = list()

    for model in models:
        bleu_score = bleu.bleu(model)
        print("BLEU: " + bleu_score)


# Evaluation
# - BLEU for beam sizes 1, 5, 10, 50
# - score for automatic translation and for reference
#   - interpretation: automatic translation will receive higher score
#                     since it gets the max values per definition
# - 3 training runs with same settings but different random seed
#   -> plot BLEU and PPL for checkpoints
#   -> what's the observation?


if __name__ == "__main__":

    # Load model
    model = torch.load(MODEL_PATH)

    # Calculate scores
    source_dev = h.extractText("multi30k_dev_de_7k")
    reference_dev = h.extractText("multi30k_dev_en_7k")
    greedy_translation = h.extractText("greedy_translation")
    
    score = calculateScores(source_dev, reference_dev, model)
    print("Score: ")
    print(score)

    # score = calculateScores(source_dev, greedy_translation, model)
    # print("Score: ")
    # print(score)

    # Translate text (greedy)
    source_text = h.extractText(inputfile)
#    translation = translate(source_text, model, "greedy", 1, True)
#    h.saveText(translation, "greedy_translation")

    # Translate text (beam)
#    translation = translate(source_text, model, "beam", beamsize, onlybest=True, post=True)
#    h.saveText(translation, "beam_translation_best_{}".format(beamsize))
