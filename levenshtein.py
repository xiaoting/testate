import numpy as np


def l_distance_matrix(reference, hypothesis):
    j = len(reference)
    k = len(hypothesis)
    l_matrix = np.zeros(shape=(j+1,k+1))
    for word in range(j + 1):
        l_matrix[word][0] = word
    for word in range(k + 1):
        l_matrix[0][word] = word
    substitution = 0
    insertion = 0
    deletion = 0
    for reference_index in range(1, j + 1):
        for hypothesis_index in range(1, k + 1):
            if (reference[reference_index-1] == hypothesis[hypothesis_index-1]):
                l_matrix[reference_index][hypothesis_index] = l_matrix[reference_index-1][hypothesis_index-1]
            else:
                substitution = l_matrix[reference_index- 1][hypothesis_index- 1]
                insertion = l_matrix[reference_index- 1][hypothesis_index]
                deletion = l_matrix[reference_index][hypothesis_index- 1]
                min_step = min(substitution, insertion, deletion)
                if min_step==substitution:
                    l_matrix[reference_index][hypothesis_index] = substitution + 1
                elif min_step==insertion:
                    l_matrix[reference_index][hypothesis_index] = insertion + 1
                else:
                    l_matrix[reference_index][hypothesis_index] = deletion + 1
    return l_matrix

def l_path(l_matrix):
    reference_index = (len(l_matrix))-1 # vertical
    hypothesis_index = (len(l_matrix[0]))-1 # horizontal
    path = []
    while (hypothesis_index>0 and reference_index>0):
            substitution = l_matrix[reference_index- 1][hypothesis_index- 1]
            insertion = l_matrix[reference_index- 1][hypothesis_index]
            deletion = l_matrix[reference_index][hypothesis_index- 1]
            min_step = min(substitution, insertion, deletion)
            if min_step==substitution:
                if (l_matrix[reference_index-1][hypothesis_index-1] == l_matrix[reference_index][hypothesis_index]):
                    path.append("match")
                else:
                    path.append("substitution")
                reference_index -= 1
                hypothesis_index -= 1
            elif min_step==insertion:
                path.append("insertion")
                reference_index -= 1
            else:
                path.append("deletion")
                hypothesis_index -= 1
    if (hypothesis_index!=0):
        path = path + hypothesis_index*["deletion"]
    elif (reference_index!=0):
        path = path + reference_index*["insertion"]
    return path
