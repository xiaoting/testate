import bpe
import sys

# input_file = sys.argv[1]
input_file = "output_de_1k_ops"

file = open(input_file)
text = file.read()
file.close()

text_out = bpe.bpe_undo(text)

output = open("rebuilt_out", "w")
output.write(text_out)
output.close()