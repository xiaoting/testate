from Dictionary import Dictionary
import numpy as np


def generateBatch(source_file: str, target_file: str, window_size: int, dict_de: Dictionary, dict_en: Dictionary):
    source_text = extractText(source_file) 
    target_text = extractText(target_file)
    lines_in_src = source_text.splitlines()
    lines_in_tgt = target_text.splitlines()
    
    start_buffer = []
    for j in range(window_size):
        start_buffer.append("<s>")
        
    end_buffer = []
    for j in range(window_size + 1):
        end_buffer.append("</s>")

    
    #add <s> and </s> for each line in source and target text
    batchlines_src = []
    batchlines_tgt = []
    for i in range(len(lines_in_src)):
        add_s_for_src = start_buffer + lines_in_src[i].split() + end_buffer
        batchlines_src.append(add_s_for_src)
    for j in range(len(lines_in_tgt)):
        add_s_for_tgt = start_buffer + lines_in_tgt[j].split() + ["</s>"]
        batchlines_tgt.append(add_s_for_tgt)
    
    #generate source
    source = []
    for line in range(len(batchlines_src)):
        for i in range(0, len(batchlines_tgt[line])-window_size):
            source_windows = []
            alg = alignment(len(batchlines_src[line]), i)
            for j in range(0, 2*window_size+1):
                if alg+2*window_size < len(batchlines_src[line])-1:
                    source_windows.append(Dictionary.getIndex(dict_de, batchlines_src[line][alg+j]))
                    #source_windows.append(batchlines_src[line][alg+j])
                else:
                    source_windows.append(Dictionary.getIndex(dict_de, batchlines_src[line][len(batchlines_src[line])-2*window_size-1+j]))
                    #source_windows.append(batchlines_src[line][len(batchlines_src[line])-2*window_size-1+j])
            source.append(source_windows)
            
    #generate target        
    target = []
    for line in range(0, len(batchlines_tgt)):
        for i in range(0, len(batchlines_tgt[line])-window_size):
            target_windows = []
            for j in range(0, window_size):
                target_windows.append(Dictionary.getIndex(dict_en, batchlines_tgt[line][i+j]))
                #target_windows.append(batchlines_tgt[line][i+j])
            target.append(target_windows)
       
    #generate label     
    label = []
    for line in range(0, len(batchlines_tgt)):
        for i in range(0, len(batchlines_tgt[line])-window_size):
            target_label = []
            target_label.append(Dictionary.getIndex(dict_en, batchlines_tgt[line][i+window_size]))
            #target_label.append(batchlines_tgt[line][i+window_size])
            label.append(target_label)
   
    return source, target, label

def alignment(src_len, index):
    if index < src_len + 1:
        b = index
    else:
        b = src_len
    return b

def extractText(filename):
    file = open(filename)
    text = file.read()
    file.close()
    return text 


# # Main
# if __name__ == "__main__":
#     dict_de = Dictionary("/Users/mac/Desktop/spp_code/multi30k.de")
#     dict_en = Dictionary("/Users/mac/Desktop/spp_code/multi30k.en")
#     batches = generateBatch("/Users/mac/Desktop/spp_code/multi30k.de", "/Users/mac/Desktop/spp_code/multi30k.en", 2, dict_de, dict_en)
    
# output = open("batches_index_optimize", "a")

# for n in range(len(batches[2])):
#     tabs0 = "     "
#     tabs1 = "     "
#     string = "{0}{1}{2}{3}{4}"
#     line = string.format(batches[0][n], tabs0, batches[1][n], tabs1, batches[2][n])
#     output.write( line )
#     output.write("\n")

# output.close()