class Dictionary:
    def __init__(self, file_name: str):
        # self.dict = list()
        self.words = dict()
        self.indices = dict()

        self.words[0] = "<s>"
        self.indices["<s>"] = 0
        self.words[1] = "</s>"
        self.indices["</s>"] = 1
        self.words[2] = "<UNK>"
        self.indices["<UNK>"] = 2

        self.nextIndex = 3
        file = open(file_name)
        for line in file:
            words = line.split()
            for word in words:
                if word not in self.words:
                    self.add(word)
 
    # Function to add string-index-pairs to vocabulary
    def add(self, word):
        if word not in self.words.values():
            self.words[self.nextIndex] = word
            self.indices[word] = self.nextIndex
            self.nextIndex += 1

        # self.dict.append(string)

    # Function to get index of string
    def getIndex(self, word):
        if word in self.words.values():
            return self.indices[word]
        else:
            return 2
        
        # for i in range(len(self.dict)):
        #     if self.dict[i] == string:
        #         return i

    # Function to get string of index
    def getWord(self, index):
        if index in self.indices.values():
            return self.words[index]
        else:
            return "<UNK>"

    # Function to generate vocabulary from text
    def generate(self, text):
        words = text.split()
        for word in words:
            if word not in self.words:
                self.add(word)

    # Function to apply vocabulary to new data set
    def apply(self, text):
        lines = text.splitlines()
        for line in range(len(lines)):
            lines[line] = lines[line].split()

        for line in lines:
            for word in line:
                if word not in self.words:
                    word = "<UNK>"

        # Rebuild text from table
        for line in lines:
            line = " ".join(line)
        text_out = "\n".join(lines)

        return text_out

    def len(self):
        return len(self.words)
