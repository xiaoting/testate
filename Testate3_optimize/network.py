from matplotlib.pyplot import connect
from numpy import source
from Dictionary import Dictionary
import batch_optimize
import torch
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
import torch.nn as nn
import torch.nn.functional as F
from torch.optim import Adam
import math

## Set up the DataSet ----------------------------------------------------
class CustomDataSet(Dataset):
    def __init__(self, source_file_name: str, target_file_name: str, dict_de, dict_en, window_size):
        batches = batch_optimize.generateBatch(source_file_name, target_file_name, window_size, dict_de, dict_en)
        self.source_data = torch.Tensor(batches[0])
        self.source_data.requires_grad = True
        self.target_data = torch.Tensor(batches[1])
        self.target_data.requires_grad = True
        self.label_data = torch.Tensor(batches[2])
        #self.label_data.requires_grad = True
    def __len__(self):
        return len(self.label_data)
    def __getitem__(self, index: int):
        return self.source_data[index], self.target_data[index], self.label_data[index]
        


# Neural network parameters
emb_dim = 100

# Training parameters
learning_rate = 0.001
lr_adaption = False

## NEURAL NETWORK ARCHITECTURE -------------------------------------------
class Network(nn.Module):
    def __init__(self):
        super(Network, self).__init__()
        
        self.source_stack = nn.Sequential(
            # Embedding layer
            nn.Embedding(len(dict_de.words), emb_dim),
            # Fully connected layer
            nn.Linear(emb_dim, 150),
            nn.ReLU(),
        )
        self.target_stack = nn.Sequential(
            # Embedding layer
            nn.Embedding(len(dict_en.words), emb_dim),
            # Fully connected layer
            nn.Linear(emb_dim, 150),
            nn.ReLU(),
        )
        self.linear_relu_stack = nn.Sequential(
            # Fully connected layer 1
            nn.Linear(600, 600),
            nn.ReLU(),
            # Fully connected layer 2
            nn.Linear(600, len(dict_en.words)),
        )
        # Softmax layer
        self.softmax = nn.Softmax(dim=1)


    def forward(self, s, t, dict_de, dict_en):      
        s = s.long()
        s = self.source_stack(s)
        
        t = t.long()
        t = self.target_stack(t)
        
        s_t_connect = torch.cat((s, t), 1)
        s_t_connect = torch.reshape(s_t_connect,(len(s),1,600))
        s_t_connect = self.linear_relu_stack(s_t_connect)
        #s_t_connect = self.softmax(s_t_connect)
        return s_t_connect


## TRAIN THE MODEL ----------------------------------------------------
def train_one_epoch(model, dataloader, dict_de, dict_en, loss_function, optimizer):
    size = len(dataloader.dataset)
    num_batches = len(dataloader)
    test_loss, correct = 0, 0
    # Calculate output
    for index, (source_data, target_data, label_data) in enumerate(dataloader):
        #calculate the prediction and loss
        label_data = label_data.to(torch.int64)
        label_data = label_data.reshape(len(source_data))
        prediction = model(source_data, target_data, dict_de, dict_en)
        prediction = prediction.reshape(len(source_data),len(dict_en.words))
        loss = loss_function(prediction, label_data)
        test_loss += loss
        correct += (prediction.argmax(1) == label_data).type(torch.float).sum().item()
            
        #backpropagation
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        
        #print the loss
        if index % 200 == 0:
            #The item() method extracts the loss’s value as a float
            loss, current = loss.item(), index * len(source_data)
            print(f"loss: {loss:>7f}  [{current:>7d}/{size:>7d}]")
    test_loss /= num_batches
    correct /= size
    perplexity = math.exp(test_loss)
    print(f"Test Error: \n Accuracy: {(100*correct):>0.1f}%, Avg loss: {test_loss:>8f} , Perplexity: {perplexity:>8f}\n")
            

   
# Main ----------------------------------------------------
if __name__ == "__main__":

    source_file = "/Users/mac/Library/CloudStorage/OneDrive-rwth-aachen.de/spp/spp_code/multi30k.de"
    target_file = "/Users/mac/Library/CloudStorage/OneDrive-rwth-aachen.de/spp/spp_code/multi30k.en"
    dict_de = Dictionary("/Users/mac/Library/CloudStorage/OneDrive-rwth-aachen.de/spp/spp_code/multi30k.de")
    dict_en = Dictionary("/Users/mac/Library/CloudStorage/OneDrive-rwth-aachen.de/spp/spp_code/multi30k.en")
    # Set up parameters
    batch_size = 200
    window_size = 1
    epochs = 10
    device = "cpu"
    print(f"Using {device} device.........")
    
    ## Load the test date in batch size
    test_data = CustomDataSet(source_file, target_file, dict_de, dict_en, window_size)
    train_dataloader= DataLoader(test_data, batch_size, shuffle=True)
    model = Network().to(device)
    
    
    #initialize the loss function and optimizer
    loss_function = nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr = learning_rate)
    
    #get the statistics
    for i in range(epochs):
         print(f"Epoch {i+1}\n-------------------------------")
         train_one_epoch(model, train_dataloader, dict_de, dict_en, loss_function, optimizer)