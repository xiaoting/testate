import levenshtein as lev
import preprocessing as pre
import numpy as np
import math

def wer_whole(pairs):
    reference,hypothesis = zip(*pairs)
    return((lev.l_distance_matrix(pre.words(reference),pre.words(hypothesis))[-1,-1])/len(reference))

def per_whole(pairs):
    reference, hypothesis = zip(*pairs)
    matches = pre.vocabulary(hypothesis).intersection(pre.vocabulary(reference))
    return (1- (matches - max(0,(len(hypothesis)-len(reference))))/len(reference))

def wer_avg(pairs):
    wer_pair =[]
    for pair in pairs:
        wer_pair.append(lev.l_distance_matrix(pair[0],pair[1])[-1,-1]/len(pair[0]))
    return sum(wer_pair)/len(wer_pair)

def per_avg(pairs):
    per_pair =[]
    for pair in pairs:
        voc_ref = pre.vocabulary(pair[1])
        voc_hyp = pre.vocabulary(pair[0])
        matches = voc_hyp.intersection(voc_ref)
        per_pair.append((  1  -  (len(matches) - max(0,(len(pair[1])-len(pair[0]))))   /len(pair[0])))
    return sum(per_pair)/len(per_pair)

    
def n_gram_precision(n,pair):
    precision_n_num = 0
    precision_n_denom = 0
    for pair_sentence in pair:
        hypothesis_n_grams = pre.n_gram(n,pair_sentence[1])
        for hypothesis_n_gram in hypothesis_n_grams:
            precision_n_num += min(pre.n_gram_match(n,pair_sentence[0],hypothesis_n_gram),pre.n_gram_match(n,pair_sentence[1],hypothesis_n_gram))
            precision_n_denom += pre.n_gram_match(n,pair_sentence[1],hypothesis_n_gram)
    return precision_n_num/precision_n_denom

def brevity_penalty(pairs):
    hypothesis_length = 0
    reference_length = 0
    for pair_sentence in pairs:
        hypothesis_length += len(pair_sentence[1])
        reference_length += len(pair_sentence[0])
    if hypothesis_length > reference_length:
        return 1
    else:
        return (math.e**(1-(reference_length/hypothesis_length)))

def bleu(n_iter,pairs,formatted=True):
    score = brevity_penalty(pairs) * np.exp(
            sum((1/n_iter)*np.log(n_gram_precision(counter+1, pairs))for counter in range(n_iter)))
    if formatted:
        return "{0:.1%}".format(score)
    return score


if __name__ == '__main__':
    source_path = r"data\newstest.de"
    target_path = r"data\newstest.en"
    hyp1_path = r"data\newstest.hyp1"
    hyp2_path = r"data\newstest.hyp2"
    hyp3_path = r"data\newstest.hyp3"
    source_text = pre.words(pre.extract(source_path))
    reference_text = pre.words(pre.extract(target_path))
    train = [source_text,reference_text]
    hyp1_text = pre.words(pre.extract(hyp1_path))
    hyp2_text = pre.words(pre.extract(hyp2_path))
    hyp3_text = pre.words(pre.extract(hyp3_path))
    hyp = [hyp1_text,hyp2_text,hyp3_text]
    test1 = list(zip(reference_text,hyp1_text))
    test2 = list(zip(reference_text,hyp2_text))
    test3 = list(zip(reference_text,hyp3_text))
        
    # # Aufgabe 1
    # for text in train:
    #     print(pre.count(text))
    print(pre.words(pre.extract(source_path)))

    # # Aufgabe 2
    # line = 1
    # print(reference_text[line], hyp1_text[line])
    # l_matrix        = lev.l_distance_matrix(pre.tokenize(reference_text[line]), pre.tokenize(hyp1_text[line]))
    # l_operations    = lev.l_path(l_matrix)
    # print(l_matrix, l_operations)

    # # Aufgabe 3

    # # Aufgabe 4
    # print('BLEU')
    # print(bleu.bleu(4,test1))   
    # print(bleu.bleu(4,test2))   
    # print(bleu.bleu(4,test3))   
    # print('PER')
    # print(per_avg(test1))       
    # print(per_avg(test2))       
    # print(per_avg(test3))       
    # print('WER')
    # print(wer_avg(test1))       
    # print(wer_avg(test2))       
    # print(wer_avg(test3))