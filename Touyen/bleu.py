import numpy as np
import math
import preprocessing as pre

def n_gram(n,sentence):
    text_n_grams = []
    for sequence in range(len(sentence)-n+1):
        text_n_grams.append(sentence[sequence:sequence+n])
    return text_n_grams

def n_gram_match(n,reference_sentence,hypothesis_n_gram):
    reference_n_grams = n_gram(n,reference_sentence)
    matches = 0
    for reference_n_gram in reference_n_grams:
        if (hypothesis_n_gram == reference_n_gram):
            matches += 1
    return matches
    
def n_gram_precision(n,pair):
    precision_n_num = 0
    precision_n_denom = 0
    for pair_sentence in pair:
        hypothesis_n_grams = n_gram(n,pair_sentence[1])
        for hypothesis_n_gram in hypothesis_n_grams:
            precision_n_num += min(n_gram_match(n,pair_sentence[0],hypothesis_n_gram),n_gram_match(n,pair_sentence[1],hypothesis_n_gram))
            precision_n_denom += n_gram_match(n,pair_sentence[1],hypothesis_n_gram)
    return precision_n_num/precision_n_denom

def brevity_penalty(pairs):
    hypothesis_length = 0
    reference_length = 0
    for pair_sentence in pairs:
        hypothesis_length += len(pair_sentence[1])
        reference_length += len(pair_sentence[0])
    if hypothesis_length > reference_length:
        return 1
    else:
        return (math.e**(1-(reference_length/hypothesis_length)))

def bleu(n_iter,pairs,formatted=True):
    score = brevity_penalty(pairs) * np.exp(
            sum((1/n_iter)*np.log(n_gram_precision(counter+1, pairs))for counter in range(n_iter)))
    if formatted:
        return "{0:.1%}".format(score)
    return score
