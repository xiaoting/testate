import preprocessing as pre
import collections

class Dictionary:
    
    def __init__(self,text):
        vocabulary = pre.vocabulary(text)
        self.word_to_int = dict((c, i) for i, c in enumerate(vocabulary))
        self.int_to_word = dict((i, c) for i, c in enumerate(vocabulary))
        # self.one_hot_enc = collections.defaultdict(int)