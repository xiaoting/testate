import re
import collections
import json
import math

def extract(text_path):
    """ Given a path to a text file, extracts contents.

    Parameters
    ----------
    text_path : str
        path to the text file

    Returns
    -------
        list of sentences within the text file
    """
    text_file = open(text_path, 'r', encoding='utf-8')
    text = []
    for line in text_file:
        text.append(line)
    text_file.close()
    return text

def tokenize(sentence,normalize=False,delimeter = ' ', ending='\n'):
    normalized_sentence = []
    for word in sentence.split(delimeter):
        word = word.replace(ending,r"<\s>")
        if normalize:
            word = word.lower()    
            if re.match(r'^[a-z]+[-\']*[a-z]+$',word):
                continue
        normalized_sentence.append(word)
    return normalized_sentence

def words(text, normalize=False, delimeter = ' '):
    """ Given a text file, create a list of words.
    
    Parameters
    ----------
    text : list
        text file segmented in sentences
    normalized: boolean
        whether text is to be (trivially) normalized
        lowercase, punctuation removal

    Returns
    -------
        list of words (sequentially, with repetitions),
        optionally, normalized
    """
    normalized_text = []
    normalized_sentence = []
    for sentence in text:
        normalized_sentence = tokenize(sentence,normalize,delimeter)
        normalized_text.append(normalized_sentence)
        normalized_sentence=[]
    return normalized_text

def alphabetize(text, normalize=False):
    alphabet = set()
    for sentence in text:
        for character in sentence.strip().replace(" ", ""):
            alphabet.add(character)
    return alphabet
    

def vocabulary(text, out_of_voc=False, normalize=False):
    """ Creates the vocabulary of a text.

    Parameters
    ----------
    text : list
        text file segmented in sentences

    Returns
    -------
        vocabulary as a set
    """
    vocabulary = set()
    for sentence in words(text,normalize=normalize):
        for word in sentence:
            vocabulary.add(word)
    if out_of_voc:
        vocabulary.add(r'<s>')
        vocabulary.add(r'<\s>')
        vocabulary.add(r'<UNK>')
    return vocabulary

def count(text):
    return sum(len(sentence) for sentence in words(text)), len(vocabulary(text)), sum(len(sentence) for sentence in text)/len(text)


def n_gram(n,sentence):
    text_n_grams = []
    for sequence in range(len(sentence)-n+1):
        text_n_grams.append(sentence[sequence:sequence+n])
    return text_n_grams

def n_gram_match(n,reference_sentence,hypothesis_n_gram):
    reference_n_grams = n_gram(n,reference_sentence)
    matches = 0
    for reference_n_gram in reference_n_grams:
        if (hypothesis_n_gram == reference_n_gram):
            matches += 1
    return matches

def _bpe_words(text):
    bpe_words = collections.defaultdict(int)
    for sentence in words(text):
        for word in sentence:
            chars = list(word)
            chars[-1] += "@@"
            bpe_words[tuple(chars)]+=1
    # dictionary {tuple:int}
    return bpe_words
    
def _bpe_pairs(pbe_words):
    pairs = collections.defaultdict(int)
    for word, frequency in pbe_words.items():
        for index in range(len(word)-1):
            pairs[word[index],word[index+1]] += frequency
    return pairs

def _bpe_merge(pair,pbe_words):
    merged = collections.defaultdict(int)
    bigram = (''.join(pair))
    for word in pbe_words:
        updated_word = []
        for index in range(len(word)):
            if word[index]==pair[1] and index !=0:
                if word[index-1]==pair[0]:
                    updated_word[-1] = bigram
                    continue
            updated_word.append(word[index])
    return merged

def _bpe_tokens(pbe_words):
    chars = collections.defaultdict(int)
    for word, freq in pbe_words.items():
        for char in word:
            chars[char] += freq
            print(chars)
    return chars

def bpe_learn(n_iter, text):
    bpe_words = _bpe_words(text)
    for _ in range(n_iter-1):
        pairs = _bpe_pairs(bpe_words)
        # print(pairs, pairs.get)
        best_pair = max(pairs, key=pairs.get)
        print(best_pair)
        bpe_words = _bpe_merge(best_pair, bpe_words)
        bpe_tokens = _bpe_tokens(bpe_words)
    return bpe_tokens



if __name__ == '__main__':
    source_text = extract(r"data\newstest.de")
    # print(_bpe_words(source_text))
    # print(_bpe_pairs(_bpe_words(source_text)))
    # print(_bpe_merge)
    print(json.dumps(bpe_learn(1000, source_text), indent = 4))
