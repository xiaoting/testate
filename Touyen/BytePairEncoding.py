import preprocessing as pre
import re
import collections

class BytePairEncoding:

    def __init__(self):
        self.record = []
        #TODO implement class structure

    def _pairs(words):
        pairs = collections.defaultdict(int)
        for word, freq in words.items():
            chars = word.split()
            for index in range(len(chars)-1):
                pairs[chars[index],chars[index+1]]+= freq
        return pairs

    def _merge(pair, words):
        merged = {}
        bigram = ''.join(pair)
        pattern = re.compile(bigram)
        for word in words:
            updated_word = pattern.sub(bigram,word)
            merged[updated_word]=words[word]
        return merged

    def _tokens(words):
        chars = collections.defaultdict(int)
        for word, freq in words.items():
            chars = word.split()
            for char in chars:
                chars[char] += freq

    def learn(self,text,n_iter):
        words = collections.defaultdict(int)
        for word in pre.words(text):
            words[' '.join(word) + ' @@'] += 1
        chars = collections.defaultdict(int)
        for word, freq in words.items():
            chars = word.split()
            for char in chars:
                chars[char] += freq  
        for _ in n_iter:
            pairs = self._pairs(words)
            best = max(pairs, key=pairs.get)
            self.record.append(best)
            words = self._merge(best,words)
        return self._tokens(words)

    def segment():
        pass
        #TODO