import matplotlib_inline
import numpy


def levenshtein(J, K):

    # Matrix namens distances erstellen mit länge J+1 und K+1 und alle Werte auf 0 setzen
    distances = numpy.zeros((len(J) + 1, len(K) + 1))

    #Füllt die Matrix in der Spalte 0 mit der länge des Wortes (0,1,2,3,...)
    for J1 in range(len(J) + 1):
        distances[J1][0] = J1


    # Füllt die Matrix in der Zeile 0 mit der länge des Wortes (0,1,2,3,...)
    for K2 in range(len(K) + 1):
        distances[0][K2] = K2

    insertion = 0
    deletion = 0
    substitution = 0

    for J1 in range(1, len(J) + 1):
        for K2 in range(1, len(K) + 1):

            #Match:  Nimm Kosten von diagonal links oben
            if (J[J1 - 1] == K[K2 - 1]):
                distances[J1][K2] = distances[J1 - 1][K2 - 1]
            else:

                insertion = distances[J1][K2 - 1]
                deletion = distances[J1 - 1][K2]
                substitution = distances[J1 - 1][K2 - 1]

                #Minimum von Substitution, Insertion und Deletion
                if (insertion <= deletion and insertion <= substitution):
                    distances[J1][K2] = insertion + 1
                elif (deletion <= insertion and deletion <= substitution):
                    distances[J1][K2] = deletion + 1
                else:
                    distances[J1][K2] = substitution + 1




    print(distances)
    distance = int(distances[len(J)][len(K)])
    print("Die Levenshtein-Distanz für die Wörter " + J + " und " + K + " ist "  + str(distance) + ".")
    return


levenshtein("ananas", "banane")

def calculateWER(file_hypothese, file_reference):
    anzahl_zeilen = 0
    WER_insg = 0
    for h1, h2 in paar_list:
        distance = levenshteinDistanceDP(h1, h2)
        WER = distance / len(1)
        WER_insg += WER
        anzahl_zeilen += 1
    durchschnittliche_WER = WER_insg / anzahl_zeilen
    return durchschnittliche_WER




def calculatePER(file_hypothese, file_reference):
    
    satzn = []
    satzj = []
    Übereinstimmung = 0
    paars = get_paar(file_hypothese, file_reference)
    for j,n in paars:
        satzj += convert_to_list(j)
        satzn += convert_to_list(n)
    for i in range(0, len(satzj),1):
        if satzj[i] in satzen:
            Übereinstimmung += 1
    PER = 1 - ((Übereinstimmung - max(0, len(file_hypothese)-len(file_reference))))/(len(file_reference))
    return PER